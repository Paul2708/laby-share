package de.paul2708.labyshare;

import de.paul2708.labyshare.listener.MessageReceiveListener;
import de.paul2708.labyshare.listener.MessageSendListener;
import de.paul2708.labyshare.message.MessageManager;
import de.paul2708.labyshare.settings.ShareSettings;
import de.paul2708.labyshare.sharebin.PasteClient;
import net.labymod.api.LabyModAddon;
import net.labymod.gui.elements.DropDownMenu;
import net.labymod.ingamegui.enums.EnumModuleAlignment;
import net.labymod.settings.elements.*;
import net.labymod.utils.Consumer;
import net.labymod.utils.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;

import java.util.List;
import java.util.Locale;

/**
 * This class represents the main addon class.
 *
 * @author Paul2708
 */
public class ShareAddon extends LabyModAddon {

    private PasteClient pasteClient;

    private ShareSettings settings;
    private MessageManager messageManager;

    /**
     * Called, if the addon was enabled.
     */
    @Override
    public void onEnable() {
        // Load settings and client
        this.messageManager = new MessageManager();
        this.settings = new ShareSettings();

        this.pasteClient = new PasteClient(this);

        // Register listener
        getApi().getEventManager().register(new MessageReceiveListener(this));
        getApi().getEventManager().register(new MessageSendListener(this));
    }

    /**
     * As it is deprecated, nothing will be done here.
     *
     * @deprecated
     */
    @Override
    public void onDisable() {
        // Nothing to do
    }

    /**
     * Called, if the config was loaded.
     */
    @Override
    public void loadConfig() {

    }

    /**
     * Fill the in-game settings.
     *
     * @param list list of settings.
     */
    @Override
    protected void fillSettings(List<SettingsElement> list) {
        // Settings
        BooleanElement enableElement = new BooleanElement("Enable", new ControlElement.IconData(Material.APPLE),
                aBoolean -> settings.enable(aBoolean), settings.isEnabled());

        StringElement tokenElement = new StringElement("API-Token", new ControlElement.IconData(Material.PAPER),
                settings.getApiToken(), accepted -> settings.setApiToken(accepted));

        BooleanElement limitElement = new BooleanElement("Paste on Limit reached", new ControlElement.IconData(Material.BARRIER),
                aBoolean -> settings.setPasteOnLimitReached(aBoolean), settings.isPasteOnLimitReached());

        BooleanElement debugElement = new BooleanElement("Debug Mode", new ControlElement.IconData(Material.STONE_PICKAXE),
                aBoolean -> settings.setDebug(aBoolean), settings.isDebug());

        // Language
        DropDownMenu<Locale> dropDownMenu = new DropDownMenu<>("Language", 0, 0, 0, 0);
        dropDownMenu.fill(MessageManager.LANGUAGES);
        dropDownMenu.setSelected(MessageManager.DEFAULT_LOCALE);

        DropDownElement<Locale> languageElement = new DropDownElement<>("Language", dropDownMenu);
        languageElement.setChangeListener(locale -> messageManager.change(locale));

        list.add(enableElement);
        list.add(tokenElement);
        list.add(limitElement);
        list.add(debugElement);
        list.add(languageElement);
    }

    /**
     * Send a message to the client.
     *
     * @param message message to send
     */
    public void sendMessage(String message) {
        if (Minecraft.getMinecraft().thePlayer != null) {
            Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText(message));
        }
    }

    /**
     * Debug a message and send it to the client, if the debug mode is enabled.
     *
     * @param message message to debug
     */
    public void debug(String message) {
        if (settings.isDebug()) {
            this.sendMessage(message);

            System.out.println(message);
        }
    }

    /**
     * Get the paste client instance.
     *
     * @return paste client instance
     */
    public PasteClient getPasteClient() {
        return pasteClient;
    }

    /**
     * Get the share settings.
     *
     * @return share settings instance
     */
    public ShareSettings getSettings() {
        return settings;
    }

    /**
     * Get the message manager instance.
     *
     * @return message manager instance
     */
    public MessageManager getMessageManager() {
        return messageManager;
    }
}
