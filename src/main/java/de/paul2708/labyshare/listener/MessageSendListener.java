package de.paul2708.labyshare.listener;

import de.paul2708.labyshare.ShareAddon;
import net.labymod.api.events.MessageSendEvent;

/**
 * This listener is called, if the player sends a message.
 *
 * @author Paul2708
 */
public class MessageSendListener implements MessageSendEvent {

    private ShareAddon addon;

    /**
     * Create a new message send listener with addon instance.
     *
     * @param addon addon instance
     */
    public MessageSendListener(ShareAddon addon) {
        this.addon = addon;
    }

    /**
     * Called, if the player sends a message.
     *
     * @param message message
     * @return true if the message is blocked, otherwise false
     */
    @Override
    public boolean onSend(String message) {
        if (!addon.getSettings().isEnabled()) {
            return false;
        }

        if (message.trim().equalsIgnoreCase(".paste")) {
            this.addon.getPasteClient().paste();

            return true;
        }

        return false;
    }
}
