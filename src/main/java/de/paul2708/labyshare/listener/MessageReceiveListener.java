package de.paul2708.labyshare.listener;

import de.paul2708.labyshare.ShareAddon;
import net.labymod.api.events.MessageReceiveEvent;

/**
 * This listener is called, if the player received a chat message.
 *
 * @author Paul2708
 */
public class MessageReceiveListener implements MessageReceiveEvent {

    private ShareAddon addon;

    /**
     * Create a new message receive listener with addon instance.
     *
     * @param addon addon instance
     */
    public MessageReceiveListener(ShareAddon addon) {
        this.addon = addon;
    }

    /**
     * Called, if the player receives a message.
     *
     * @param colorLessMessage received message without colors
     * @param message received message
     * @return false if the player receives it, otherwise true (blocking)
     */
    @Override
    public boolean onReceive(String colorLessMessage, String message) {
        if (replaceColorCodes(message).trim().startsWith(replaceColorCodes(addon.getMessageManager().getPrefix()))) {
            return false;
        }

        this.addon.getPasteClient().addMessage(message);
        return false;
    }

    /**
     * Remove all color codes (<code>§</code>).
     *
     * @param message message with color codes
     * @return message without color codes
     */
    private String replaceColorCodes(String message) {
        return message.replaceAll("§([a-z]|[0-9]|[A-Z])", "");
    }
}
