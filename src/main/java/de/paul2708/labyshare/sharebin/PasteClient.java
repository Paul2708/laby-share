package de.paul2708.labyshare.sharebin;

import de.paul2708.client.SharebinAPI;
import de.paul2708.client.client.AsyncSharebinClient;
import de.paul2708.client.client.util.Result;
import de.paul2708.client.exception.ErrorResponseException;
import de.paul2708.common.paste.Paste;
import de.paul2708.common.rest.response.ErrorResponse;
import de.paul2708.labyshare.ShareAddon;
import net.labymod.utils.ServerData;
import net.minecraft.client.Minecraft;
import net.minecraft.event.ClickEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.IChatComponent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * This class holds the {@link AsyncSharebinClient} to store and paste messages.
 *
 * @author Paul2708
 */
public class PasteClient {

    /**
     * Default api token (none token).
     */
    public static final String DEFAULT_TOKEN = "*insert token here*";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy - HH:mm");

    private static final String NEW_LINE = "\n";

    private static final String BASE_URL = "https://sbin.tk?id=";

    private static final int MAX_LENGTH = Paste.MAX_LENGTH - 1000;

    private static final String ERROR_URL = "https://bitbucket.org/Paul2708/sharebin-api/src/master/common/";

    private ShareAddon addon;

    private List<String> messages;
    private int length;

    /**
     * Create a new paste client with the addon instance.
     *
     * @param addon addon instance
     */
    public PasteClient(ShareAddon addon) {
        this.addon = addon;

        this.messages = new LinkedList<>();
        this.length = 0;
    }

    /**
     * Add a message to the local storage. Before adding, the color codes will be removed by
     * {@link #replaceColorCodes(String)}.
     *
     * @param message input chat message
     */
    public void addMessage(String message) {
        if (!addon.getSettings().isEnabled()) {
            return;
        }
        String colorLessMessage = replaceColorCodes(message);

        if (addon.getSettings().isPasteOnLimitReached() && length + colorLessMessage.length() > PasteClient.MAX_LENGTH) {
            addon.sendMessage(addon.getMessageManager().get("auto_paste.limit_reached"));

            paste();
            return;
        }

        this.messages.add(colorLessMessage);
        this.length += colorLessMessage.length();
    }

    /**
     * Paste the current storage and clear it afterwards.
     */
    public void paste() {
        if (!addon.getSettings().isEnabled()) {
            return;
        }

        if (messages.isEmpty() || length == 0) {
            this.addon.sendMessage(addon.getMessageManager().get("paste.no_content"));
            return;
        }

        this.addon.sendMessage(addon.getMessageManager().get("paste.process"));

        // Build content
        StringBuilder builder = new StringBuilder();

        builder.append(header()).append(PasteClient.NEW_LINE);

        for (String message : messages) {
            builder.append(message).append(PasteClient.NEW_LINE);
        }

        builder.append(footer());

        // Paste content
        AsyncSharebinClient client = SharebinAPI.createAsync(addon.getSettings().getApiToken());

        client.paste(builder.toString(), new Result<Paste>() {

            @Override
            public void result(Paste paste) {
                IChatComponent comp = new ChatComponentText(addon.getMessageManager().get("paste.url", "url",
                        PasteClient.BASE_URL + paste.getCode()));
                ChatStyle style = new ChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,
                        PasteClient.BASE_URL + paste.getCode()));
                comp.setChatStyle(style);

                Minecraft.getMinecraft().thePlayer.addChatMessage(comp);

                messages.clear();
                length = 0;
            }

            @Override
            public void error(ErrorResponseException e) {
                ErrorResponse response = e.getErrorResponse();

                IChatComponent comp = new ChatComponentText(addon.getMessageManager().get("paste.error", "error",
                        response.getDescription()));
                ChatStyle style = new ChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL,
                        PasteClient.ERROR_URL));
                comp.setChatStyle(style);

                Minecraft.getMinecraft().thePlayer.addChatMessage(comp);

                addon.debug("Error: " + response.getError() + "(id=" + response.getId() + ")");
                addon.debug("Description: " + response.getDescription());
            }
        });
    }

    /**
     * Build the header with server and time information.
     *
     * @return header
     */
    private String header() {
        StringBuilder builder = new StringBuilder();

        builder.append("--- Chat history for ");

        ServerData serverData = this.addon.getApi().getCurrentServer();
        if (serverData == null || (serverData.getIp().equalsIgnoreCase("localhost")
                || serverData.getIp().equalsIgnoreCase("127.0.0.1"))) {
            builder.append("Singleplayer");
        } else {
            builder.append(serverData.getIp());
        }

        builder.append(" --- at ").append(PasteClient.DATE_FORMAT.format(new Date()));

        return builder.toString();
    }

    /**
     * Build the footer.
     *
     * @return footer
     */
    private String footer() {
        return "--- End ---";
    }

    /**
     * Remove all color codes (<code>§</code>).
     *
     * @param message message with color codes
     * @return message without color codes
     */
    private String replaceColorCodes(String message) {
        return message.replaceAll("§([a-z]|[0-9]|[A-Z])", "");
    }
}
