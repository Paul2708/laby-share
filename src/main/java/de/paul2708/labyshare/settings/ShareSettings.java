package de.paul2708.labyshare.settings;

import de.paul2708.labyshare.sharebin.PasteClient;

/**
 * This class holds the settings for this addon.
 *
 * @author Paul2708
 */
public class ShareSettings {

    private boolean enabled;

    private String apiToken;

    private boolean pasteOnLimitReached;
    private boolean pasteOnQuit;

    private boolean debug;

    /**
     * Create new settings with default values (enabled, default api token, paste on limit reached, paste on quit,
     * disabled debug mode).
     */
    public ShareSettings() {
        this.enabled = true;

        this.apiToken = PasteClient.DEFAULT_TOKEN;

        this.pasteOnLimitReached = true;
        this.pasteOnQuit = true;

        this.debug = false;
    }

    /**
     * Check if the addon is enabled.
     *
     * @return true if the addon is enabled, otherwise false
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Enable or disable the addon.
     *
     * @param enabled enable (true) or disable (false)
     */
    public void enable(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Get the api token.
     *
     * @return api token
     */
    public String getApiToken() {
        return apiToken;
    }

    /**
     * Set the api token.
     *
     * @param apiToken api token
     */
    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    /**
     * Check if auto-paste is enabled, if the paste limit is reached.
     *
     * @return true if the option is enabled, otherwise false
     */
    public boolean isPasteOnLimitReached() {
        return pasteOnLimitReached;
    }

    /**
     * Enable or disable the auto-paste, if the paste limit is reached.
     *
     * @param pasteOnLimitReached enable (true) or disable (false)
     */
    public void setPasteOnLimitReached(boolean pasteOnLimitReached) {
        this.pasteOnLimitReached = pasteOnLimitReached;
    }

    /**
     * Check if auto-paste is enabled, if the player quits the server.
     *
     * @return true if the option is enabled, otherwise false
     */
    public boolean isPasteOnQuit() {
        return pasteOnQuit;
    }

    /**
     * Enable or disable the auto-paste, if the player quits the server.
     *
     * @param pasteOnQuit enable (true) or disable (false)
     */
    public void setPasteOnQuit(boolean pasteOnQuit) {
        this.pasteOnQuit = pasteOnQuit;
    }

    /**
     * Check if the debug mode is enabled.
     *
     * @return true if the debug mode is enabled, otherwise false
     */
    public boolean isDebug() {
        return debug;
    }

    /**
     * Enable or disable the debug mode.
     *
     * @param debug enable (true) or disable (false)
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
