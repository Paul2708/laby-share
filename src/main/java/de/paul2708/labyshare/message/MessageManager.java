package de.paul2708.labyshare.message;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This class is the message manager and handles the different language files.
 *
 * @author Paul2708
 */
public class MessageManager {

    /**
     * Default locale.
     */
    public static final Locale DEFAULT_LOCALE = Locale.GERMANY;

    /**
     * Array of all possible languages.
     */
    public static final Locale[] LANGUAGES = new Locale[] { Locale.US, Locale.GERMANY };

    private static final String[] TOKEN = new String[] { "%", "%" };

    private ResourceBundle resourceBundle;
    private String prefix;

    /**
     * Create a new message manager and apply the default locale.
     */
    public MessageManager() {
        this.change(MessageManager.DEFAULT_LOCALE);
    }

    /**
     * Get a message with prefix.
     *
     * @param key message key
     * @return message value
     */
    public String get(String key) {
        return prefix + replaceColorCodes(resourceBundle.getString(key));
    }

    /**
     * Get a message with parameters.
     *
     * @param key message key
     * @param parameter parameters
     * @return message value with parameters
     */
    public String get(String key, String... parameter) {
        String message = this.get(key);

        for (int i = 0; i < parameter.length - 1; i++) {
            message = message.replace(MessageManager.TOKEN[0] + parameter[i] + MessageManager.TOKEN[1],
                    parameter[i + 1]);
        }

        return message;
    }

    /**
     * Change the locale to another one and update the resource bundle.
     *
     * @param newLocal new locale
     */
    public void change(Locale newLocal) {
        this.resourceBundle = ResourceBundle.getBundle("language/messages", newLocal);
        this.prefix = replaceColorCodes(resourceBundle.getString("prefix"));
    }

    /**
     * Get the current prefix.
     *
     * @return prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Replace all color codes to display it.
     *
     * @param message message with color codes
     * @return message with applied color codes
     */
    private String replaceColorCodes(String message) {
        return message.replaceAll("&", "\u00a7");
    }
}
